use std::collections::HashMap;

/// Struct containg AppInfo? Sent with many requests
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct AppInfo {
    /// App Id?
    pub id: String,
    /// Server?
    pub bundle: String,
    /// App Version?
    pub ver: String,

    /// Unknown/Extra options
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}
