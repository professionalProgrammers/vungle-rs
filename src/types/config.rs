use std::collections::HashMap;
use url::Url;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Config {
    pub endpoints: Endpoints,
    pub placements: Vec<Placement>,
    pub config: serde_json::Value,
    pub will_play_ad: serde_json::Value,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Endpoints {
    pub new: Url,
    pub ads: Url,
    pub ri: Url,
    pub report_ad: Url,
    pub log: Url,
    pub will_play_ad: Url,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Placement {
    pub id: String,
    pub reference_id: String,
    pub is_auto_cached: bool,
    pub is_incentivized: bool,
    pub supported_template_types: Option<Vec<String>>,
    pub supported_ad_formats: Option<Vec<String>>,
    pub cache_priority: Option<u32>,
    pub header_bidding: Option<bool>,
    pub ad_refresh_duration: Option<u32>,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}
