use std::collections::HashMap;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct SystemInfo {
    pub cache: Vec<serde_json::Value>,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

impl Default for SystemInfo {
    fn default() -> Self {
        SystemInfo {
            cache: Vec::new(),
            extra: HashMap::new(),
        }
    }
}
