use std::collections::HashMap;

/// Device Info. Sent with most requests
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct DeviceInfo {
    /// Device make
    pub make: String,
    /// Device version
    pub os: String,
    /// Device Useragent
    pub ua: String,
    /// Device Height?
    pub h: u64,
    /// Device Model
    pub model: String,
    /// Device OS Version
    pub osv: String,
    /// Extra data?
    pub ext: serde_json::Value,
    /// Device Carrier
    pub carrier: String,
    /// Purpose Unknown
    pub ifa: String,
    /// Device Width
    pub w: u64,
    /// Purpose Unknown
    pub lmt: u64,
    /// Extra / Unknown Fields
    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

/// Extra IOS Info.
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct IosInfo {
    /// Volume
    pub volume: u32,
    /// Battery State
    pub battery_state: String,
    pub idfa: String,
    pub vduid: String,
    /// Battery Level
    pub battery_level: f64,
    pub locale: String,
    pub connection_type: String,
    pub language: String,
    pub storage_bytes_available: u64,
    pub idfv: String,
    pub battery_saver_enabled: u32,
    pub time_zone: String,
}
