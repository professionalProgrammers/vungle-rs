use std::collections::HashMap;

/// User Info
#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct UserInfo {
    pub vision: Option<Vision>,
    pub gdpr: GdprInfo,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct GdprInfo {
    pub consent_timestamp: u64,
    pub consent_status: String,
    pub consent_message_version: String,
    pub consent_source: String,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Vision {
    pub data_science_cache: String,
    pub aggregate: Vec<Aggregate>,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

impl Vision {
    pub fn new() -> Self {
        Vision {
            data_science_cache: String::new(),
            aggregate: Vec::new(),
            extra: HashMap::new(),
        }
    }
}

impl Default for Vision {
    fn default() -> Self {
        Vision::new()
    }
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Aggregate {
    pub creative_details: Vec<serde_json::Value>,
    pub campaign_details: Vec<serde_json::Value>,
    pub advertiser_details: Vec<serde_json::Value>,
    pub last_viewed_creative_id: String,
    pub window: u64,
    pub total_view_count: u64,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}
