pub mod client;
pub mod error;
pub mod types;

pub use crate::{
    client::Client,
    types::{
        device_info::IosInfo,
        user_info::{
            GdprInfo,
            Vision,
        },
        AppInfo,
        DeviceInfo,
        GetAdRequest,
        GetConfigRequest,
        SystemInfo,
        UserInfo,
    },
};
pub use serde_json::json;

#[cfg(test)]
mod test {
    use super::*;
    use std::collections::HashMap;

    fn get_app_info() -> AppInfo {
        AppInfo {
            id: "5ad8f49f2366ab644c5ff33f".into(),
            bundle: "com.wtfapps.apollo16".into(),
            ver: "1.39.1.1".into(),

            extra: HashMap::new(),
        }
    }

    fn get_system_info() -> SystemInfo {
        SystemInfo::default()
    }

    fn get_device_info() -> DeviceInfo {
        DeviceInfo {
            make: "Apple".into(),
            os: "ios".into(),
            ua: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148".into(),
            h: 1334,
            model: "iPhone10,4".into(),
            osv: "13.5.1".into(),
            ext: json!({
                "vungle": {
                    "ios": IosInfo {
                        volume: 0,
                        battery_state: "unplugged".into(),
                        idfa: "00000000-0000-0000-0000-000000000000".into(),
                        vduid: String::new(),
                        battery_level: 0.33000001311302185,
                        locale: "en_US".into(),
                        connection_type: "wifi".into(),
                        language: "en-US".into(),
                        storage_bytes_available: 3821182976,
                        idfv: "D1DEC993-70B6-4F92-A94C-AA55A813868F".into(),
                        battery_saver_enabled: 0,
                        time_zone: "America/Los_Angeles".into(),
                    },
                }
            }),
            carrier: "AT&T".into(),
            ifa: "00000000-0000-0000-0000-000000000000".into(),
            w: 750,
            lmt: 1,
            extra: HashMap::new(),
        }
    }

    fn get_user_info() -> UserInfo {
        UserInfo {
            vision: None,
            gdpr: GdprInfo {
                consent_timestamp: 1596784324,
                consent_status: "opted_in".into(),
                consent_message_version: String::new(),
                consent_source: "publisher".into(),

                extra: HashMap::new(),
            },
            extra: HashMap::new(),
        }
    }

    #[tokio::test]
    async fn it_works() {
        let app_info = get_app_info();
        let system_info = get_system_info();
        let device_info = get_device_info();
        let user_info = get_user_info();

        let client = Client::new();
        let _config = client
            .get_config(GetConfigRequest {
                app_info: &app_info,
                system_info: &system_info,
                device_info: &device_info,
                user_info: &user_info,
            })
            .await
            .unwrap();
    }
}
