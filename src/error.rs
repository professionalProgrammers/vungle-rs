pub type VungleResult<T> = Result<T, VungleError>;

#[derive(Debug)]
pub enum VungleError {
    Reqwest(reqwest::Error),
    Json(serde_json::Error),
    InvalidStatus(reqwest::StatusCode),
    Io(std::io::Error),
}

impl From<reqwest::Error> for VungleError {
    fn from(e: reqwest::Error) -> Self {
        VungleError::Reqwest(e)
    }
}

impl From<serde_json::Error> for VungleError {
    fn from(e: serde_json::Error) -> Self {
        VungleError::Json(e)
    }
}

impl From<std::io::Error> for VungleError {
    fn from(e: std::io::Error) -> Self {
        VungleError::Io(e)
    }
}
