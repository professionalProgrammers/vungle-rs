use crate::{
    error::{
        VungleError,
        VungleResult,
    },
    types::{
        AdsResponse,
        Config,
        GetAdRequest,
        GetConfigRequest,
    },
};
use flate2::{
    write::GzEncoder,
    Compression,
};
use reqwest::header::{
    ACCEPT,
    ACCEPT_ENCODING,
    ACCEPT_LANGUAGE,
    CONNECTION,
    CONTENT_ENCODING,
    CONTENT_LENGTH,
    CONTENT_TYPE,
    HOST,
    USER_AGENT,
};
use serde_json::json;
use std::io::Write;

const CONFIG_URL: &str = "https://ads.api.vungle.com/config";

const VUNGLE_VERSION: &str = "Vungle-Version";
const VUNGLE_BUNDLE_ID: &str = "X-VUNGLE-BUNDLE-ID";

const HOST_VALUE: &str = "ads.api.vungle.com";
const CONTENT_TYPE_VALUE: &str = "application/json";
const USER_AGENT_VALUE: &str = "Vungle/6.5.3;native";
const ACCEPT_LANGUAGE_VALUE: &str = "en-us";
const VUNGLE_VERSION_VALUE: &str = "5.4.0";

pub struct Client {
    pub client: reqwest::Client,
}

impl Client {
    pub fn new() -> Self {
        Client {
            client: reqwest::Client::new(),
        }
    }

    pub async fn get_config(&self, request: GetConfigRequest<'_>) -> VungleResult<Config> {
        let payload = json!({
            "app": request.app_info,
            "system": request.system_info,
            "device": request.device_info,
            "ext": {},
            "request": {},
            "user": request.user_info
        });

        let payload_str = serde_json::to_string(&payload)?;

        let req = self
            .client
            .post(CONFIG_URL)
            .header(HOST, HOST_VALUE)
            .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
            .header(ACCEPT, "*/*")
            .header(CONNECTION, "keep-alive")
            .header(VUNGLE_BUNDLE_ID, request.app_info.bundle.as_str())
            .header(VUNGLE_VERSION, VUNGLE_VERSION_VALUE)
            .header(USER_AGENT, USER_AGENT_VALUE)
            .header(CONTENT_LENGTH, payload_str.len().to_string())
            .header(ACCEPT_LANGUAGE, ACCEPT_LANGUAGE_VALUE)
            .header(ACCEPT_ENCODING, "gzip, deflate, br")
            .body(payload_str);

        let res = req.send().await?;

        let status = res.status();
        if !status.is_success() {
            return Err(VungleError::InvalidStatus(status));
        }

        let text = res.text().await?;
        let json = serde_json::from_str(&text)?;

        Ok(json)
    }

    pub async fn get_ad(&self, request: GetAdRequest<'_>) -> VungleResult<AdsResponse> {
        let payload = json!({
            "app": request.app_info,
            "system": request.system_info,
            "device": request.device_info,
            "ext": {},
            "request": {
                "header_bidding": false,
                "placements": request.placements,
                "ad_size": "unknown"
            },
            "user": request.user_info
        });

        let payload_str = serde_json::to_string(&payload)?;
        let mut encoder = GzEncoder::new(Vec::new(), Compression::default());
        encoder.write_all(payload_str.as_bytes())?;
        let payload_bytes = encoder.finish()?;

        let req = self
            .client
            .post(request.config.endpoints.ads.as_str())
            .header(HOST, HOST_VALUE)
            .header(CONTENT_TYPE, CONTENT_TYPE_VALUE)
            .header(CONTENT_ENCODING, "gzip")
            .header(CONTENT_LENGTH, payload_bytes.len().to_string())
            .header(VUNGLE_BUNDLE_ID, request.app_info.bundle.as_str())
            .header(VUNGLE_VERSION, VUNGLE_VERSION_VALUE)
            .header(USER_AGENT, USER_AGENT_VALUE)
            .header(ACCEPT_LANGUAGE, ACCEPT_LANGUAGE_VALUE)
            .body(payload_bytes);

        let res = req.send().await?;
        let status = res.status();

        if !status.is_success() {
            return Err(VungleError::InvalidStatus(status));
        }

        let text = res.text().await?;
        let json = serde_json::from_str(&text)?;

        Ok(json)
    }
}

impl Default for Client {
    fn default() -> Self {
        Client::new()
    }
}
