use serde_json::json;
use std::{
    collections::HashMap,
    io::Read,
    path::PathBuf,
};
use vungle::{
    client::Client,
    types::{
        device_info::IosInfo,
        user_info::{
            GdprInfo,
            Vision,
        },
        AppInfo,
        DeviceInfo,
        GetAdRequest,
        GetConfigRequest,
        SystemInfo,
        UserInfo,
    },
};

fn main() {
    let mut rt = match tokio::runtime::Runtime::new() {
        Ok(rt) => rt,
        Err(e) => {
            eprintln!("Failed to init tokio runtime: {}", e);
            return;
        }
    };

    rt.block_on(async_main());

    println!("Done. Press [Enter] to continue.");
    let _ = std::io::stdin().lock().read(&mut [0]).is_ok();
}

async fn async_main() {
    let vungle_client = Client::new();
    let ads_dir = PathBuf::from("ads");

    match std::fs::create_dir_all(&ads_dir) {
        Ok(_) => {
            println!("Created ad dir");
        }
        Err(e) => {
            eprintln!("Failed to create ad dir, got: {}", e);
            return;
        }
    }

    // Start Create vungle client data
    let app_info = AppInfo {
        id: "5ad8f49f2366ab644c5ff33f".into(),
        bundle: "com.wtfapps.apollo16".into(),
        ver: "1.39.1.1".into(),

        extra: HashMap::new(),
    };

    let system_info = SystemInfo::default();

    let device_info = DeviceInfo {
        make: "Apple".into(),
        os: "ios".into(),
        ua: "Mozilla/5.0 (iPhone; CPU iPhone OS 13_5_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148".into(),
        h: 1334,
        model: "iPhone10,4".into(),
        osv: "13.5.1".into(),
        ext: json!({
            "vungle": {
                "ios": IosInfo {
                    volume: 0,
                    battery_state: "unplugged".into(),
                    idfa: "00000000-0000-0000-0000-000000000000".into(),
                    vduid: String::new(),
                    battery_level: 0.33000001311302185,
                    locale: "en_US".into(),
                    connection_type: "wifi".into(),
                    language: "en-US".into(),
                    storage_bytes_available: 3821182976,
                    idfv: "D1DEC993-70B6-4F92-A94C-AA55A813868F".into(),
                    battery_saver_enabled: 0,
                    time_zone: "America/Los_Angeles".into(),
                },
            }
        }),
        carrier: "AT&T".into(),
        ifa: "00000000-0000-0000-0000-000000000000".into(),
        w: 750,
        lmt: 1,
        extra: HashMap::new(),
    };

    let mut user_info = UserInfo {
        vision: None,
        gdpr: GdprInfo {
            consent_timestamp: 1596784324,
            consent_status: "opted_in".into(),
            consent_message_version: String::new(),
            consent_source: "publisher".into(),

            extra: HashMap::new(),
        },
        extra: HashMap::new(),
    };

    // End Create Vungle Client Data

    let config = match vungle_client
        .get_config(GetConfigRequest {
            app_info: &app_info,
            system_info: &system_info,
            device_info: &device_info,
            user_info: &user_info,
        })
        .await
    {
        Ok(config) => {
            println!("Loaded Config");
            config
        }
        Err(e) => {
            eprintln!("Failed to get config: {:#?}", e);
            return;
        }
    };

    let placements: Vec<_> = config
        .placements
        .iter()
        .map(|p| p.reference_id.to_string())
        .collect();

    println!("Located Placements: ");
    for placement in placements.iter() {
        println!("{}", placement);
    }
    println!();

    let mut current = 0;
    let mut last_url = None;
    let mut new_ads = Vec::new();

    // user_info.vision = Some(Vision::sample_data());
    user_info.vision = Some(Vision::new());

    loop {
        loop {
            let placement_payload = vec![placements[current].clone()];

            match vungle_client
                .get_ad(GetAdRequest {
                    config: &config,
                    placements: &placement_payload,
                    user_info: &user_info,
                    app_info: &app_info,
                    device_info: &device_info,
                    system_info: &system_info,
                })
                .await
            {
                Ok(data) => {
                    assert!(data.ads.len() == 1);

                    let template_settings = data
                        .ads
                        .first()
                        .and_then(|ad| ad.ad_markup.template_settings.as_ref());

                    match template_settings {
                        Some(template_settings) => {
                            let current_url =
                                &template_settings.cacheable_replacements.main_video.url;
                            dbg!(current_url);

                            let filename = current_url.path_segments().unwrap().last().unwrap();
                            let file_path = ads_dir.join(filename);

                            if !file_path.exists() {
                                let video = vungle_client
                                    .client
                                    .get(current_url.as_str())
                                    .send()
                                    .await
                                    .unwrap()
                                    .bytes()
                                    .await
                                    .unwrap();
                                std::fs::write(file_path, video).unwrap();
                                new_ads.push(current_url.to_string());
                            }

                            if let Some(url) = last_url {
                                if &url == current_url {
                                    last_url = None;
                                    current += 1;
                                    println!("Advancing Placement");
                                } else {
                                    last_url = Some(current_url.clone());
                                }
                            } else {
                                last_url = Some(current_url.clone());
                            }
                        }
                        None => {
                            if let Some(ad) = data.ads.first() {
                                if let Some(sleep) = ad.ad_markup.sleep {
                                    println!("Sleep Code: {}", sleep);
                                }

                                if let Some(info) = ad.ad_markup.info.as_ref() {
                                    println!("Info: {}", info);
                                }
                            } else {
                                panic!("Missing First Ad");
                            }
                            current += 1;
                            println!("Advancing Placement");
                        }
                    }
                }

                Err(e) => {
                    eprintln!("Error: {:#?}", e);
                    println!("Advancing Placement");
                    current += 1;
                }
            }

            if current >= placements.len() {
                break;
            }
        }

        println!("Got {} new ads: ", new_ads.len());
        if !new_ads.is_empty() {
            for ad in new_ads.iter() {
                println!("{}", ad);
            }

            break;
        }

        current = 0;
        last_url = None;
        new_ads.clear();
    }
}
