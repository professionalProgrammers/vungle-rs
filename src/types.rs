pub mod app_info;
pub mod config;
pub mod device_info;
pub mod system_info;
pub mod user_info;

pub use self::{
    app_info::AppInfo,
    config::Config,
    device_info::DeviceInfo,
    system_info::SystemInfo,
    user_info::UserInfo,
};
use std::collections::HashMap;
use url::Url;

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct AdsResponse {
    pub ads: Vec<Ad>,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct Ad {
    pub placement_reference_id: String,
    pub ad_markup: AdMarkup,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct AdMarkup {
    pub id: String,
    pub sleep: Option<u32>,
    pub info: Option<String>,

    #[serde(rename = "templateSettings")]
    pub template_settings: Option<TemplateSettings>,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct TemplateSettings {
    pub cacheable_replacements: CacheableReplacements,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CacheableReplacements {
    #[serde(rename = "MAIN_VIDEO")]
    pub main_video: CacheableReplacementsUrl,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct CacheableReplacementsUrl {
    pub extension: String,
    pub url: Url,

    #[serde(flatten)]
    pub extra: HashMap<String, serde_json::Value>,
}

#[derive(Debug)]
pub struct GetConfigRequest<'a> {
    pub app_info: &'a AppInfo,
    pub system_info: &'a SystemInfo,
    pub device_info: &'a DeviceInfo,
    pub user_info: &'a UserInfo,
}

#[derive(Debug)]
pub struct GetAdRequest<'a> {
    pub config: &'a Config,
    pub placements: &'a [String],
    pub user_info: &'a UserInfo,
    pub app_info: &'a AppInfo,
    pub system_info: &'a SystemInfo,
    pub device_info: &'a DeviceInfo,
}
