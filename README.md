# vungle-rs

[![Build status](https://ci.appveyor.com/api/projects/status/4vc6926bqx0b8q3f?svg=true)](https://ci.appveyor.com/project/professionalProgrammers/vungle-rs)
[![](https://tokei.rs/b1/bitbucket.org/professionalProgrammers/vungle-rs)](https://bitbucket.org/professionalProgrammers/vungle-rs)

A Rust API for Vungle.